/**
 * @module config
 */
// Параметры соединения
exports.prod = {
    serv_worker : 'localhost:50052',
    serv_static : 'localhost:40052',
    serv_graphql : 'localhost:30052',
    startWsPort:7000,
    startHttpPort:8888
};
exports.local = {
    serv_worker : 'localhost:50052',
    serv_static : 'localhost:40052',
    serv_graphql : 'localhost:30052',
    startWsPort:3333,
    startHttpPort:8888
};
exports.evgeniy = {
    serv_worker : '192.168.102.231:50052',
    serv_static : '192.168.102.231:40052',
    serv_graphql : '192.168.102.231:30052',
    startWsPort:7000,
    startHttpPort:8888
};
exports.default = {
    serv_worker : 'localhost:50052',
    serv_static : 'localhost:40052',
    serv_graphql : 'localhost:30052',
    startWsPort:7000,
    startHttpPort:8888
};
