var Server = function(){}
Server.prototype.async = require('async');
Server.prototype.cluster = require('cluster');
Server.prototype.ws = require('ws');
Server.prototype.os = require('os');
Server.prototype.fs = require('fs');
Server.prototype.controllers = new Array()
Server.prototype.server_util = require('./app/modules/server_util')();
Server.prototype.start = function(){
    global.Server.cluster.isMaster? global.type = 'master': global.type = 'worker'


    var self = this;
    this.async.series([
            function (callback) {
                // Модуль выдающий ошибку по номеру в требуемом формате
                global.getError = require('./app/modules/errors');
                callback(null,'Error module: [OK]');
                // После выполнения у global появляется общий для всех метод getError. (global.getError)
            },
            function (callback) {
                // Инициализируем cache, загрузка конфига сервера в систему cache
                self.cacheModule = require('./app/modules/cache');
                callback(null,'Module Cache + Config: [OK]');
            },
            // function (callback) {
            //     // Загрузка маршрутизатора запросов
            //     self.router = require('./app/modules/router');
            //     //self.router.initialize();
            //     callback(null, 'Request Router: [OK]');
            // },
            function (callback) {
                // Загрузка контроллеров инициализация воркеров

                    require('./master/modules/'+global.type).initialize(callback)

                    //callback(null,'WORKER INIT')
            },

        ],
        // Callback для последовательного исполнения
        function (err,results) {
            console.log("<--------process------->",global.type)
            if(err) {
                console.log(err);
            } else {

                for(var i = 0; i < results.length; i++)
                {

                    global.type=='master'?console.info("[MASTER]",results[i]):null
                    global.type=='worker'?console.info("[CLUSTER]",results[i]):null
                }
                global.type=='master'?console.info("[START SERVER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!]",global.config):null






            }
        }
    );
}

module.exports = Server


