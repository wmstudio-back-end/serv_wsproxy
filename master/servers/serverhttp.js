var http = require("http");
var querystring = require('querystring');
var urlf  = require('url');
var config = global.gfmaster.cacheModule.getConfig('PAYMENTS_SERVER_SETTINGS');
function echo_cross_domain(responder){
    responder.writeHead(200, {'Content-Type': 'application/xml; charset=UTF-8'});
    responder.write('<?xml version="1.0"?>');
    responder.write('<!DOCTYPE cross-domain-policy SYSTEM "http://www.adobe.com/xml/dtds/cross-domain-policy.dtd">');
    responder.write('<cross-domain-policy>');
    responder.write('<allow-access-from domain="*" to-ports="*"/>');
    responder.write('</cross-domain-policy>');
    responder.end();
}
module.exports = function (callback){
    function onRequest(request, responder){
        var fullBody = '';
        var url = request.url.split('/');
        var url_parts = urlf.parse(request.url,true);
        var querydata = url_parts.query;

        request.on('data', function(chunk){
            fullBody += chunk.toString();
        });
        request.on('end', function(){
            var decodedBody = querystring.parse(fullBody);
            var platform = null;
            if(url[1] === "crossdomain.xml"){
                echo_cross_domain(responder);
                return;
            }

            if(url.hasOwnProperty('2')){
                switch(url[2]) {
                    case 'ym':
                        platform = require('../controllers/ym');
			console.log('YM pay');
                        if (querydata.hasOwnProperty('eventtype') && querydata.eventtype === 'event.joingroup') {
                            platform.joingroup(querydata, function (data) {
                                responder.writeHead(200, { 'Content-Type': 'application/json' });
                                responder.write(data);
                                responder.end();
                            });
			    console.log(querydata);
                            return;
                            break;
                        }

                        if (fullBody) {
                            var body = JSON.parse(fullBody);
                        } else {
                            body = {
                                order_id : querydata.order_id
                            };
                        }

                        var sig = request.headers['authorization'];
                        if (sig) body.sig = sig;

                        body.user_id = 'mbga.jp:' + querydata.opensocial_viewer_id;
                        platform.payments(body, function (data) {
                            console.log(data);
                            console.log(platform.generateSignature(data));
                            responder.writeHead(200, {
                                'Content-Type': 'application/json',
                                'X-MBGA-PAYMENT-SIGNATURE' : platform.generateSignature(data)
                            });
                            responder.write(data);
                            responder.end();
                        });
                        return;
                        break;
                    case 'vk':
                        platform = require('../controllers/vk');
                        break;
                    case 'ok':


                        var req = {error:"product_code"};
                        if (querydata.hasOwnProperty('product_code')) {
                            if (global.gfmaster.cacheModule.getStaticCollections().packages.hasOwnProperty(querydata.product_code)) {
                                if (querydata.amount == global.gfmaster.cacheModule.getStaticCollections().packages[querydata.product_code].cost) {
                                    platform = require('../controllers/ok');
                                    platform.payments(querydata, responder);
                                    req = {"result":true};
                                }
                            }
                        }
                        if (req.hasOwnProperty('error'))
                        {
                            responder.writeHead(200, {'Content-Type': 'application/json'});
                            responder.write(req);
                            responder.end();
                        }



                        return;
                        break;
                    case 'nk':
                        platform = require('../controllers/nk');
                        platform.payments(decodedBody,function(data){
                            responder.writeHead(200, {'Content-Type': 'application/json'});
                            responder.write(data);
                            responder.end();
                        });
                        return;
                        break;
                    case 'fs':
                        platform = require('../controllers/fs');
                        platform.payments(decodedBody, function(data){
                            responder.writeHead(200, {'Content-Type': 'application/json'});
                            responder.write(data);
                            responder.end();
                        });
                        return;
                        break;
                    case 'mail':
                        platform = require('../controllers/mailru');
                        platform.payments(querydata,function(data){
                            responder.writeHead(200, {'Content-Type': 'application/json'});
                            responder.write(data);
                            responder.end();
                        });
                        return;
                        break;
                    case 'qz':
                        platform = require('../controllers/qz');
                        console.log('qz response', url);
                        if (url[3] === 'proxy' || url[3] === 'getToken' || url[3] === 'payment') {
                            var method = platform[url[3]] || platform.payment;
                            method(decodedBody, querydata, function (data) {
                                responder.writeHead(200, {
                                    'Content-Type': 'application/json',
                                    'Access-Control-Allow-Origin' : '*'
                                });
                                responder.write(data);
                                responder.end();
                            });
                            return;
                            break;
                        }

                        return;
                        break;
                    case 'fb':
                        platform = require('../controllers/fb');

                        if(url.hasOwnProperty('3')){
                            if (url[3] == 'story') {
                                return platform.getStory(request.headers, request.url, url[4], url[5], responder);
                            }
                            if (url[3] == 'Graph') {

                                return platform.getGraph(request.headers, request.url, url, responder);
                            }
                            if(url[3] == 'payments'){
                                if(fullBody == ''){
                                    platform.payments(url[4],responder);
                                } else {
                                    platform.payments(JSON.parse(fullBody),responder);
                                }
                            } else {
                                platform.getPackInfo(url[3],responder);
                            }
                        }
                        return;
                        break;
                }
            }
            if(!platform || !global.gfmaster.status){
                responder.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
                responder.write("нет такого запроса ");
                responder.write("---");
                responder.end();
                return;
            }
            platform.payments(decodedBody,function(data){
                responder.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
                responder.write(data);
                responder.end();
            });
        });
    }
    http.createServer(onRequest).listen(config.PORT);
    callback(null,"Payments server [OK]. Listening PORT : "+config.PORT);

};
