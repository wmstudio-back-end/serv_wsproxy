exports.initialize = initialize;

function worker_controllers(callback) {
    global.Server.fs.readdir('master/controllers/worker', function (err, files) {
        global.Server.worker_controllers = new Array()

        for (var ActFile in files) {

            var t = files[ActFile]
            //console.log("ssssssssssssssss",t)
            global.Server.worker_controllers[files[ActFile].split('.')[0]] = {
                method: require(global.dirRoot + '/master/controllers/worker/' + files[ActFile]),
                path: require.resolve(global.dirRoot + '/master/controllers/worker/' + files[ActFile])
            }
        }

        callback(null, 'worker_controllers INIT')
    })
}
function grpc_controllers(callback) {



/////////////////////////////
    var PROTO_PATH = global.dirRoot + '/proto/worker_service.proto';
    var grpc = require('grpc');
    var _proto = grpc.load(PROTO_PATH).worker_service;

    var wsproxy = {
        User : require(global.dirRoot + '/master/controllers/client/User'),
        Message : require(global.dirRoot + '/master/controllers/client/Message')
    }

    global.Server.client_controller = {}
    global.Server.client_controller.wsproxy = {
        dataSend:function(data,resp){
            var d = JSON.parse(data.data)
            var req = d.method.split('/');
            if (wsproxy.hasOwnProperty(req[0])){
                if (wsproxy[req[0]].hasOwnProperty(req[1])){
                    wsproxy[req[0]][req[1]](d,resp)
                }else{
                    console.log('NOT Method')
                }
            }else{
                console.log('NOT CONTROLLER')
            }

            //         console.log('resp')
            //require(global.dirRoot + '/master/controllers/worker/User')
        }
    }
    // {
    //     dataSend : function(data,resp){
    //         console.log("==========================")
    //         console.log('data',data)
    //         console.log('resp')
    //         resp(null,{data:JSON.stringify({dataasdasd:{}})})
    //     }}
    global.Server.client_controller.graphql = new _proto.ServiceWorker(global.config.serv_graphql_host+":"+global.config.serv_graphql_port,
            grpc.credentials.createInsecure());
        global.Server.client_controller.worker = new _proto.ServiceWorker(global.config.serv_worker_host+":"+global.config.serv_worker_port,
            grpc.credentials.createInsecure());
        callback(null, 'client_controllers INIT')


}
function ready_controllers(callback) {
    process.send({
        method: 'ready',
        data: {},
    });
    callback(null, 'READY CONTROLLERS [OK]')
}
function initialize(callback) {
    global.Server.async.series([worker_controllers, grpc_controllers, grpcInit, ready_controllers], function (err, results) {
        callback(null, 'WORKER CONTROLLER INIT')
    })


}


process.on('message', (msg) => {
    var req = msg.method.split('/');
    if (!global.Server.worker_controllers.hasOwnProperty(req[0])) {
        console.log("NOT WORKER CONTROLLER")
        return;
    }

    if (!global.Server.worker_controllers[req[0]].method.hasOwnProperty(req[1])) {
        console.log("NOT WORKER METHOD")
        return;
    }

    global.Server.worker_controllers[req[0]].method[req[1]](msg['data'])
});


function grpcInit(callback) {
    callback(null, 'WORKER CONTROLLER INIT')


}





