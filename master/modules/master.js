function messageHandler(msg) {
    switch (msg.method){
        case 'ready':

            send(this.id,{
                id:this.id,
                port:global.config.serv_wsproxy_wsport+this.id
            })

            break;

    }

}
function send(id,data){
    global.Server.cluster.workers[id].send({
        method:'init/socketRouter',
        worker:id,
        data:data,
    })
}


function initialize(callback){
    const numCPUs = require('os').cpus().length;
    global.Server.workers = new Array()
    for (let i = 0; i < numCPUs; i=i+2) {
        global.Server.workers.push(global.Server.cluster.fork());
    }
    for (var id in global.Server.cluster.workers) {
        global.Server.cluster.workers[id].on('message', messageHandler);
    }
    callback(null,'MASTER : [OK]');
}

exports.initialize = initialize;