var cookie = require('cookie');
var cookieParser = require('cookie-parser')
var OPTIONS = false
var ws = require('ws');
var https = require('https');
var WebSocketServer = require("ws");

var http = require('http');
var fs = require('fs');
var path = require('path');
var qs = require('querystring');

function httpServer(request, response){
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    response.setHeader('Access-Control-Allow-Credentials', true); // If needed

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var setcookie = true
            var type_profile = null;
            if (post['type-register']!==undefined){
                if (post['type-register']=='applicant'){
                    type_profile = 'Student'
                }
                if (post['type-register']=='employer'){
                    type_profile = 'Company'
                }
                if (post['type-register']=='educator'){
                    type_profile = 'Educator'
                }
            }
            if (post['login']!==undefined&&post['password']!==undefined){
                var data = {
                    method:'Authentication/auth',
                    data: {
                        email: post['login'],
                        password: post['password'],

                    },
                    requestId:1
                }
                global.Server.client_controller.worker.dataSend({data:JSON.stringify(data)},function(err, res) {
                    var dataResp = {}
                    if (err){
                        dataResp.error = {
                            text:"nologin",
                            code:1002
                        }
                        console.log(err)
                    }else{

                        dataResp = JSON.parse(res.data)

                    }
                    response.end(JSON.stringify(dataResp))
                })

            }

            if (
                type_profile!=null
                &&post[post['type-register']+'-firstName']!==undefined
                &&post[post['type-register']+'-lastName']!==undefined
                &&post[post['type-register']+'-mail']!==undefined
                &&post[post['type-register']+'-pass']!==undefined
                &&post[post['type-register']+'-repeatPass']!==undefined
            ){




                post['type_profile'] = type_profile
                var data = {
                    method:'Authentication/reg',
                    //method_controller:'worker/Authentication/reg',
                    data:post,
                    // data: {
                    //     first_name: post[post['type-register']+'-firstName'],
                    //     last_name: post[post['type-register']+'-lastName'],
                    //     password:post[post['type-register']+'-pass'],
                    //     email:post[post['type-register']+'-mail'],
                    //     type_profile:type_profile,
                    //     post:post
                    //
                    // },
                    requestId:1
                }
                var dataResp = {}
                if (post[post['type-register']+'-pass']!=post[post['type-register']+'-repeatPass']){
                    dataResp.error = {
                        text:"incorrect password",
                        code:1002
                    }
                    response.end(JSON.stringify(dataResp))
                }

                global.Server.client_controller.worker.dataSend({data:JSON.stringify(data)},function(err, res) {
                    if (err){
                        if (err.code==11000){
                            dataResp.error = {
                                text:"dublicate email",
                                code:1001
                            }
                        }

                    }else{
                        //response.setHeader('set-cookie', 'token='+JSON.parse(res.data).profile._id)

                        dataResp = JSON.parse(res.data)

                    }
                    response.end(JSON.stringify(dataResp))

                })
            }

        });

    }
}
function getOptions(){
    var data = {
        data: null,
        requestId: 1,
        method: 'System/getoptions',
        method_controller: 'worker/System/getoptions'
    }

    global.Server.client_controller.worker.dataSend({data:JSON.stringify(data)},function(err, res) {
        OPTIONS = JSON.parse(res.data).data.options
        console.log('_+_++++++++++++++++++++++++++++++++++++++')
    })
}

function socketRouter(data){

    getOptions()

    http.createServer(httpServer).listen(global.config.serv_wsproxy_httpport);

    var wsServer = new WebSocketServer.Server({port:data.port});
    wsServer.on("connection", function connection(ws, req) {
        var SESSION_ID = "UID_"+new ObjectID().toString();
        ws.SESSION_ID = SESSION_ID
        var cookies = {}
        if (ws.upgradeReq.headers.cookie){
            cookies=cookie.parse(ws.upgradeReq.headers.cookie);
        }
        if (cookies.hasOwnProperty('token'))
        {
            var data = {
                data: { token: cookies.token },
                requestId: 1,
                method: 'Authentication/tokenprofile',
                method_controller: 'worker/Authentication/tokenprofile'
            }
            global.Server.client_controller.worker.dataSend({data:JSON.stringify(data)},function(err, res) {


                res.data = JSON.parse(res.data).data

                setUser(res.data.profile,ws)
            })
        }






        //ws.send(JSON.stringify(data));
        ws.on("message", function (msg) {

            process_message(ws,msg)
        });
        ws.on('close', socket_close);
    });
}
var tytyt = 0;
function setUser(user,ws){
    console.log("SEEEEET USER ",user._id)
    ws.id=user._id

    var mess = {
        error: null,
        method: 'wsproxy/User/Connect',
        data: {
            uid: user._id
        }
    }
    if (!global.User_subscribe.hasOwnProperty(user._id)){
        global.User_subscribe[user._id] = []
    }

    for (var i=0;i<global.User_subscribe[user._id].length;i++){
        if (global.users.hasOwnProperty(global.User_subscribe[user._id][i])){
            global.sendUser(global.User_subscribe[user._id][i],mess)
            // for(var y=0;y<global.users[global.User_subscribe[user._id][i]].socket.length;y++){
            //     global.users[global.User_subscribe[user._id][i]].socket[y].send(JSON.stringify(mess))
            // }

        }

    }


    if (!global.users.hasOwnProperty(user._id)){
        global.users[user._id] = {}
        global.users[user._id].profile = user
        global.users[user._id].socket = {}
        global.users[user._id].socket[ws.SESSION_ID] = ws
        global.users[user._id].User_subscribe = []

    }else if (global.users.hasOwnProperty(user._id)){
        global.users[user._id].profile = user
        global.users[user._id].socket[ws.SESSION_ID] = ws


    }
    var mess = {
        error : null,
        method: 'worker/Authentication/identify',
        requestId : 1,
        data:{
            options:OPTIONS,
            profile:global.users[user._id].profile
        }
    }

     ws.send(JSON.stringify(mess));




}
global.sendUser = function(id,data){
    for(var session in global.users[id].socket){

        global.users[id].socket[session].send(JSON.stringify(data))
    }
}
MergeRecursive = function(obj1, obj2) {

    for (var p in obj2) {
        try {
            // Property in destination object set; update its value.
            if ( obj2[p].constructor==Object ) {
                obj1[p] = MergeRecursive(obj1[p], obj2[p]);

            } else {
                obj1[p] = obj2[p];

            }

        } catch(e) {
            // Property in destination object not set; create it and set its value.
            obj1[p] = obj2[p];

        }
    }

    return obj1;
}

global.setProfile = function (id,data) {
    global.users[id].profile = MergeRecursive(global.users[id].profile,data)
    console.log(global.users[id].profile)
}

// setInterval(function(){
//
//         var user  = {
//             USER:Object.keys(global.users),
//
//         }
//         console.log("ONLINE USERS",user)
//
//
//
// },1000)
function socket_close(e) {
    //global.users.User_subscribe
    if (this.hasOwnProperty('id')&&global.users.hasOwnProperty(this.id)) {

        var mess = {}
        mess.error = null
        mess.method = 'wsproxy/User/Disconnect'
        mess.data = {uid: this.id}
        global.Server.client_controller.worker.dataSend({data:JSON.stringify({
            method:'User/disconnectUser',
            method_controller : 'User/disconnectUser',
            user_id : this.id,
            data : {_id: this.id}
        })},function(err, response) {

        })
        // for (var i = 0; i < global.users[this.id].User_subscribe.length; i++) {
        //     if (global.users.hasOwnProperty(global.users[this.id].User_subscribe[i])) {
        //         global.users[global.users[this.id].User_subscribe[i]].socket.send(JSON.stringify(mess))
        //     }
        // }
        console.log("CLOSE",this.id)
        for (var session in global.users[this.id].socket){
                delete global.users[this.id].socket[this.SESSION_ID]
                console.log("##########################",this.SESSION_ID)

        }
        if (Object.keys(global.users[this.id].socket).length==0){
            delete global.users[this.id]
            console.log("DELETE ALL")

            for (var i=0;i<global.User_subscribe[this.id].length;i++){
                if (global.users.hasOwnProperty(global.User_subscribe[this.id][i])){
                    global.sendUser(global.User_subscribe[this.id][i],mess)


                }

            }
        }

    }

}
function userIdentify(ws,token,message){

    var data = {
        data: {},
        requestId:message.requestId
    }

    if (message!=null){
        var req = message.method.split('/');
        data.method =  req[1]+"/"+req[2]
        data.method_controller = message.method
        data.data = message.data
    }

    if (token!=null){
        data.method =  'worker/Authentication/identify'
        data.method_controller = 'worker/Authentication/identify'
        data.data = {_id:token}
    }





        global.Server.client_controller.worker.dataSend({data:JSON.stringify(data)},function(err, response) {
            if (err)
            {
                // response.data = null
                // response.error = err
                // response.method= 'worker/Authentication/identify',
                //     ws.send(JSON.stringify(response));
                console.log(err)
                return;
            }

            message.requestId?response.requestId = message.requestId:message.requestId = 1
            if (response.data!=null){
                response.data = JSON.parse(response.data).data
                if (!OPTIONS){
                    OPTIONS = response.data.options
                }

                // console.log("response",response.data)
                if (response.data.profile){
                    setUser(response.data.profile,ws)
                }
                else if(response.data.error){

                    response.error = response.data.error
                    response.data = null
                    response.method= 'worker/Authentication/identify',
                     ws.send(JSON.stringify(response));
                }


                //ws.send(JSON.stringify(response));



            }else{
                response.data = null
                response.error = err
                response.method= 'worker/Authentication/identify',
                    ws.send(JSON.stringify(response));
            }


        });

}
function process_message(sock, message)
{


  message = JSON.parse(message);

        if(!message.hasOwnProperty('method')|| !message.hasOwnProperty('data')) {return;}

        if(global.Server.needToStopServer) {
            var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
            call_client(sock,server_error,{},global.getError(5));
            return;
        }

        var req = message.method.split('/');

        if (!global.Server.client_controller.hasOwnProperty(req[0])) {
            console.log("NOT CLIENT CONTROLLER")
            return;
        }




        //message.params.player = global.gfserver.cacheModule.getPlayer(sock.ppid);
        //нет в кеше, пишем сокет для авторицазии
        // if(!message.params.player){
        //     if(req[0] !== 'authentication'&&req[0] !== 'reauthentication'){
        //         //давай досвиданья
        //         console.log('try to play without auth');
        //         call_client(sock,global.gfserver.router.clientControllers.client_methods['server_error'],{},global.getError(6));
        //         return;
        //     }
        //     message.sock = sock; // Передаем сокет для метода авторизации.
        // }

        //message.sock = sock;





    if (!sock.hasOwnProperty('id')){
        if (req[1]=='Authentication'){

            userIdentify(sock,null,message);
        }
        return
    }else if (req[1]=='Authentication'){
        userIdentify(sock,null,message);
        return
    }
    message.method_controller = message.method
    message.method = req[1]+"/"+req[2]
    message.requestId = message.requestId
    message.user_id = sock.id


        global.Server.client_controller[req[0]].dataSend({data:JSON.stringify(message)},function(err, response) {

            var data = JSON.parse(response.data)
            if (data.hasOwnProperty('request')){
                data.method = data.request
                process_message(sock, JSON.stringify(data))

            }
                response.data = data.data
                response.error = err
                response.requestId = message.requestId
                response.method = message.method_controller
                sock.send(JSON.stringify(response));




        });


        return
        //     global.Server.grpc_controllers.worker_service[req[0]][req[1]](message.data,function(err,results){
        //
        //         console.log('message.data',message.data)
        //         console.log(err)
        //         console.log(results)
        //         // Готовим ответ и передаем его в модуль Network.
        //         //console.log('send to client prepare_answer')
        //         sock.send(prepare_answer(err, results, message.method));
        //
        //
        //     });
        // try{ }catch (err){
        //     console.log("ERROR IN DATA")
        //     //console.log(err)
        //     sock.send(prepare_answer(global.getError(6), {}, message.method));
        // }
    try
    {
    }
    catch (e){
        console.log('<- ERROR START ------------------------------------------------------------------------------>');
        console.log(e.message);
        console.log(e.stack);
        console.log('Ошибка расшифровки сообщения')
        console.log(message);
        console.log('Веремя - ppid - ip');
        console.log(new Date() + ' - '+sock.ppid + ' - '+sock.remoteAddress);
        console.log('Данные буфера');
        console.log(sock.data);
        console.log("Размер буффера");
        console.log(sock.bufferSize);
        console.log('<- ERROR END -------------------------------------------------------------------------------->');
    }
}
function prepare_answer(err, results, method){
    var t = new Date().getTime();
    if(err){
        results = global.Server.server_util.reportError(method,err);
    } else {
        results = global.Server.server_util.reportSuccess(method,results);
    }

    /* if(config.encrypt){
     var respLength = Buffer.byteLength(JSON.stringify(results), 'utf8');
     var ba = new Buffer(respLength);
     results = crypt.encrypt(ba);
     } else {*/
    results = JSON.stringify(results);
    // }
    // console.log('Method', method, 'prepare_answer took', new Date().getTime() - t,'ms' );
    return results;
}
module.exports.socketRouter = socketRouter
