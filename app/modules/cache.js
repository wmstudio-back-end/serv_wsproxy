/**
 * Модуль работы с данными в памяти
 * @module cache
 */
var async = require('async');
var CONFIG = require('../../config');


exports.getConfig = getConfig;
/**
 * Инициализация модуля
 */
function initialize() {

}
function getConfig(section) {
    // Если конфиг загружен
    if(CONFIG !== undefined)
    {
        // Если указана секция конфига и она существует возвращаем только ее
        if(section !== undefined && CONFIG[section] !== undefined)
        {
            return CONFIG[section];
        }
        // Если секция не указана или ее нет возвращаем весь конфиг
        return CONFIG;
    } else {
        // Если конфиг не загружен (возможно стоит его загрузить), но пока FALSE
        return false;
    }
}