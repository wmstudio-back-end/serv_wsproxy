var errors_data = require('../../app/system/errors_data');

module.exports = function(id){
    var classError = function(id){
        this.errorId = id;
        this.message = errors_data[id] || "Undefined Error";
    };

    classError.prototype.toString = function(){
        return this.message;
    };

    return new classError(id);
};