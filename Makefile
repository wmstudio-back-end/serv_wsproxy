.PHONY  : build proto data

all     : test build

test    :
	go test -v `go list ./... | grep -v /vendor/`

build   : gfserver.js
	mkdir -p ./build
	env CGO_ENABLED=0 node build -o build/`basename ${PWD}`

run     : gfserver.js
	npm start

proto       :
	mkdir -p ./src/pb
	cd ./proto && protoc --js_out=library=grpc:pb *.proto


data        :
	mkdir -p ./src/data
	go-bindata -o src/data/data.go -prefix data -pkg data data
