Array.prototype.unique = function(){
    'use strict';
    var im = {}, uniq = [];
    for (var i=0;i<this.length;i++){
        var type = (this[i]).constructor.name,
            //          ^note: for IE use this[i].constructor!
            val = type + (!/num|str|regex|bool/i.test(type)
                    ? JSON.stringify(this[i])
                    : this[i]);
        if (!(val in im)){uniq.push(this[i]);}
        im[val] = 1;
    }
    return uniq;
}
module.exports = function() {
    var self = this;
    self.obj = function()
    {
        this.success = true;
        this.data = {};
        this.errors = [];
        this.requestId = 0;
    };
    self.logPackageSchema = function()
    {
        this.user_id=0;
        this.app_id=0;
        this.date=0;
        this.item='';
        this.cost = 0;
        this.item_title = '';
        this.order_id = 0;
        this.platform_type = '';
    };
    self.reportError = function (requestId, errorMessage) {
        var result = new self.obj();
        result.success = true;
        result.requestId = -1;
        result.methodId = global.gfserver.router.clientControllers.client_methods['server_error'];
        result.data = {message : errorMessage.toString()};
        return result;
    };
    self.reportSuccess = function (requestId, data) {
        var result = new self.obj();
        result.data = data;
        result.requestId = requestId;
        return result;
    };
    
    self.clientPacket = function (methodId, data) {
        var result = new self.obj();
        result.data = data;
        result.requestId = -1;
        result.methodId = methodId;
        return result;
    };

    self.serverError = function (methodId, errorMessage) {
        var result = new self.obj();
        result.requestId = -1;
        result.methodId = methodId;
        result.data = {message:errorMessage.toString()}
        return result;
    };
  
    self.random_string = function (len) 
    {
        var key = '';
        var keys = [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','i',
                    'j','k','l','m','n','o','p','q','r','s','t','u','v','w',
                    'x','y','z'];
    
        for (var i = 0; i < len; i++) {
            key += keys[Math.floor(Math.random()*keys.length)];
        }
    
        return key;
    };
    
    self.gracefullStop = function() {
        global.gfserver.needToStopServer = true;
        var queue = global.gfserver.cacheModule.getSaveQueue();
        var haveUsers = false;
        for(var player in queue){
            haveUsers = true;
            global.gfserver.disconectProcess.playersCount += 1;
            //Попробуем закрыть сокет а сохранение сработает автоматом при его закрытии <- херовая идея, т.к
            //игрок может нахоится на другой вкладке в браузере, в этом случае риск что сокет не закроеться до закрытия сервера         
            global.gfserver.router.kick(queue[player],666,function(ppid){
            	//Статус отключений ведется у плеера, хотя логично было бы перенести сюда 
            });
        }
        if(!haveUsers){
            var data = {
                "saved": global.gfserver.disconectProcess.savedUsers,
                "residue": global.gfserver.disconectProcess.playersCount,
                "process":process.pid
            }
            global.gfserver.router.masterControllers.worker_requests.request("mortality","worker_ready_for_death",data,null);
        }

    };
    
    return self;
};